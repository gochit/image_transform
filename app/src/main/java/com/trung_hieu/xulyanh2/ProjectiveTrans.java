package com.trung_hieu.xulyanh2;

import com.trung_hieu.xulyanh2.ControlPoint;
import android.graphics.Bitmap;

/**
 * Created by Trung-Hieu on 11/11/2014.
 */
public class ProjectiveTrans {
    public double h1;
    public double h2;
    public double h3;
    public double h4;
    public double h5;
    public double h6;
    public double h7;
    public double h8;
    public double h9;
    public Bitmap ProjectiveTransSample(Bitmap src) {
        h1=0.8335;
        h2=0.5876;
        h3=-0.4203;
        h4=0.0004;
        h5=0.6925;
        h6=0.3079;
        h7=0.0004;
        h8=0.0004;
        h9=1.0000;

        Bitmap newBitmap = Bitmap.createBitmap(src);
        int xMax = newBitmap.getWidth();
        int yMax = newBitmap.getHeight();
        // Fill with black
        for (int x=0; x<xMax; x++) {
            for (int y=0;y<yMax;y++) {
                newBitmap.setPixel(x,y,0x00000000);
            }
        }
        int color;
        int tempX,tempY;
        for (int x=0;x<xMax;x++) {
            for (int y=0;y<yMax;y++) {
                tempX=(int)Math.round((h1*x+h2*y+h3)/(h7*x+h8*y+h9));
                tempY=(int)Math.round((h4*x+h5*y+h6)/(h7*x+h8*y+h9));
                if ((tempX<xMax) && (tempY<yMax) && (tempX>0) && (tempY>0)) {
                    color = src.getPixel(x,y);
                    newBitmap.setPixel(tempX,tempY,color);
                }
                //newBitmap.setPixel(x,y,color);
            }
        }
        return newBitmap;
    }
}
