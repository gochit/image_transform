package com.trung_hieu.xulyanh2;

/**
 * Created by Trung-Hieu on 11/11/2014.
 */
public class ControlPoint {
    public int length;
    public int[] x;
    public int[] y;

    public void initCp(int newLength) {
        this.length=newLength;
        x= new int[newLength];
        y= new int[newLength];
    }

}
