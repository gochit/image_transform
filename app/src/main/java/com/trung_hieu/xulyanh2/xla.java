package com.trung_hieu.xulyanh2;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;

import android.app.Activity;
import android.os.Bundle;
import android.database.Cursor;
import android.os.Environment;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MenuInflater;
import android.content.Intent;
import android.view.View;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Bitmap.Config;
import android.widget.ImageView;
import android.provider.MediaStore;
import android.widget.TextView;
import android.net.Uri;
import com.trung_hieu.xulyanh2.AffineTrans;
import com.trung_hieu.xulyanh2.ProjectiveTrans;
import com.trung_hieu.xulyanh2.PolynomialTrans;



public class xla extends Activity {
    private ImageView mImageView;
    private Bitmap mBitmap;
    private boolean picReady;
    private static final int SELECT_PICTURE = 1;
    private static final int PICK_FROM_CAMERA = 1;
    private static final int PICK_FROM_FILE = 2;
    private Uri mImageCaptureUri;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_xla);
        picReady = false;
        mImageView = (ImageView) findViewById(R.id.imageView);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.xla, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.Projective ) {
            ProjectiveTrans projectiveTrans = new ProjectiveTrans();
            Bitmap newBitmap = Bitmap.createBitmap(mBitmap);
            //newBitmap = convertToMutable(newBitmap);
            newBitmap = projectiveTrans.ProjectiveTransSample(mBitmap);
            mImageView.setImageBitmap(newBitmap);
            return true;
        }
        if (id == R.id.Affine) {
            AffineTrans affineTrans = new AffineTrans();
            Bitmap newBitmap = Bitmap.createBitmap(mBitmap);
            //newBitmap = convertToMutable(newBitmap);
            newBitmap = affineTrans.AffineTransSample(mBitmap);
            mImageView.setImageBitmap(newBitmap);
            return true;
        }
        if (id== R.id.Poly) {
            PolynomialTrans polynomialTrans = new PolynomialTrans();
            Bitmap newBitmap = Bitmap.createBitmap(mBitmap);
            //newBitmap = convertToMutable(newBitmap);
            newBitmap = polynomialTrans.PolyTransSample(mBitmap);
            mImageView.setImageBitmap(newBitmap);
            return true;
        }
        if (id == R.id.takePhoto) {
            if (getPicture()) {
                picReady=true;
            }
            return true;
        }
        if (id == R.id.browsePhoto) {
            browserPicture();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    static final int REQUEST_IMAGE_CAPTURE = 1;

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode != RESULT_OK) return;
        String path="";
        if (requestCode == PICK_FROM_FILE) {
            mImageCaptureUri = data.getData();
            path = getRealPathFromURI(mImageCaptureUri);
            if (path == null)
                path = mImageCaptureUri.getPath();
            if (path!=null)
                mBitmap = BitmapFactory.decodeFile(path);

        }
        else {
            path	= mImageCaptureUri.getPath();
            mBitmap  = BitmapFactory.decodeFile(path);
        }
        Bitmap mutableBitmap=convertToMutable(mBitmap);
        //mImageView.setImageBitmap(mBitmap);
        if (mutableBitmap.getHeight()>OUTSIZE || mutableBitmap.getWidth()>OUTSIZE) {
            mBitmap = Bitmap.createBitmap(OUTSIZE,OUTSIZE, Config.ARGB_8888);
            mBitmap = resize512(mutableBitmap);
        }
        else {
            mBitmap = mutableBitmap;
        }
        TextView text = (TextView) findViewById(R.id.textView2);
        text.setText(String.valueOf(mBitmap.getWidth()+"x"+mBitmap.getHeight()));
        mImageView.setImageBitmap(mBitmap);
//        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
//            Bundle extras = data.getExtras();
//            mBitmap = (Bitmap) extras.get("data");
//            mImageView.setImageBitmap(mBitmap);
//            TextView text = (TextView) findViewById(R.id.textView2);
//            text.setText(String.valueOf(mBitmap.getWidth()+"x"+mBitmap.getHeight()));
//        }
    }

    public String getRealPathFromURI(Uri contentUri) {
        String [] proj = {MediaStore.Images.Media.DATA};
        @SuppressWarnings("deprecation")
        Cursor cursor = managedQuery( contentUri, proj, null, null,null);
        if (cursor == null) return null;
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }

    private boolean getPicture() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        File file = new File(Environment.getExternalStorageDirectory(),
                "tmp_avatar_"+String.valueOf(System.currentTimeMillis())+".jpg");
        mImageCaptureUri = Uri.fromFile(file);
        try {
            intent.putExtra(android.provider.MediaStore.EXTRA_OUTPUT, mImageCaptureUri);
            intent.putExtra("return-data", true);
            startActivityForResult(intent, PICK_FROM_CAMERA);
        } catch (Exception e) {
            e.printStackTrace();
        }
        //dispatchTakePictureIntent();
        return true;
    }

    private boolean browserPicture() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent,"Complete action using"),PICK_FROM_FILE);
        return true;
    }

    public static Bitmap convertToMutable(Bitmap imgIn) {
        try {
            //this is the file going to use temporally to save the bytes.
            // This file will not be a image, it will store the raw image data.
            File file = new File(Environment.getExternalStorageDirectory() + File.separator + "temp.tmp");

            //Open an RandomAccessFile
            //Make sure you have added uses-permission android:name="android.permission.WRITE_EXTERNAL_STORAGE"
            //into AndroidManifest.xml file
            RandomAccessFile randomAccessFile = new RandomAccessFile(file, "rw");

            // get the width and height of the source bitmap.
            int width = imgIn.getWidth();
            int height = imgIn.getHeight();
            Config type = imgIn.getConfig();

            //Copy the byte to the file
            //Assume source bitmap loaded using options.inPreferredConfig = Config.ARGB_8888;
            FileChannel channel = randomAccessFile.getChannel();
            MappedByteBuffer map = channel.map(FileChannel.MapMode.READ_WRITE, 0, imgIn.getRowBytes()*height);
            imgIn.copyPixelsToBuffer(map);
            //recycle the source bitmap, this will be no longer used.
            imgIn.recycle();
            System.gc();// try to force the bytes from the imgIn to be released

            //Create a new bitmap to load the bitmap again. Probably the memory will be available.
            imgIn = Bitmap.createBitmap(width, height, type);
            map.position(0);
            //load it back from temporary
            imgIn.copyPixelsFromBuffer(map);
            //close the temporary file and channel , then delete that also
            channel.close();
            randomAccessFile.close();

            // delete the temp file
            file.delete();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return imgIn;
        }
    public final int OUTSIZE=512;
    public Bitmap resize512(Bitmap src) {
        // Get the dimensions of the View
        int targetW = src.getWidth();
        int targetH = src.getHeight();
        int scaleFactor = Math.min(targetW/OUTSIZE, targetH/OUTSIZE);
        int newW = targetW/scaleFactor;
        int newH = targetH/scaleFactor;
        Bitmap resized = Bitmap.createScaledBitmap(src,newW,newH,true);
        Bitmap retBitmap = Bitmap.createBitmap(OUTSIZE,OUTSIZE, Config.ARGB_8888);
        for (int x=-256;x<255;x++) {
            for (int y=-256;y<255;y++) {
                int color = resized.getPixel(newW/2+x,newH/2+y);
                retBitmap.setPixel(256+x,256+y,color);
            }
        }
        return retBitmap;
    }
}
