package com.trung_hieu.xulyanh2;

import com.trung_hieu.xulyanh2.ControlPoint;
import android.graphics.Bitmap;
/**
 * Created by Trung-Hieu on 11/12/2014.
 */
public class PolynomialTrans {
    public double a0,a1,a2,a3,a4,a5;
    public double b0,b1,b2,b3,b4,b5;

    public Bitmap PolyTransSample(Bitmap src) {
        a0 = -321.4215;
        a1 = 1.6198;
        a2 = 0.5165;
        a3 = -0.0010;
        a4 = 0.0000;
        a5 = 0.0002;
        b0 = 0;
        b1 = 0;
        b2 = 1;
        b3 = 0;
        b4 = 0;
        b5 = 0;

        Bitmap newBitmap = Bitmap.createBitmap(src);
        int xMax = newBitmap.getWidth();
        int yMax = newBitmap.getHeight();
        // Fill with black
        for (int x=0; x<xMax; x++) {
            for (int y=0;y<yMax;y++) {
                newBitmap.setPixel(x,y,0x00000000);
            }
        }
        int color;
        int tempX,tempY;
        for (int x=0;x<xMax;x++) {
            for (int y=0;y<yMax;y++) {
                tempX=(int)Math.round((a0+a1*x+a2*y+a3*x*y+a4*x*x+a5*y*y));
                tempY=(int)Math.round((b0+b1*x+b2*y+b3*x*y+b4*x*x+b5*y*y));
                if ((tempX<xMax) && (tempY<yMax) && (tempX>0) && (tempY>0)) {
                    color = src.getPixel(tempX,tempY);
                    newBitmap.setPixel(x,y,color);
                }
                //newBitmap.setPixel(x,y,color);
            }
        }
        return newBitmap;
    }
}
