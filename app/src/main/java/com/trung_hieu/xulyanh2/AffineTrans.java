package com.trung_hieu.xulyanh2;

import com.trung_hieu.xulyanh2.ControlPoint;
import android.graphics.Bitmap;
/**
 * Created by Trung-Hieu on 11/11/2014.
 */
public class AffineTrans {
    public double a00;
    public double a01;
    public double b00;
    public double a10;
    public double a11;
    public double b11;

    public void getAffine(ControlPoint moving, ControlPoint fixed) {
        /*
         *  x'(0)= a00*x(0)+a01*y(0)+b00
         *  y'(0)= a10*x(0)+a11*y(0)+b11
         */

    }

    public Bitmap AffineTransSample(Bitmap src) {
        a00=0.6820;
        a01=0.3180;
        a10=0.0753;
        a11=0.9247;
        b00=-0.2502;
        b11=0.2502;
        Bitmap newBitmap = Bitmap.createBitmap(src);
        int xMax = newBitmap.getWidth();
        int yMax = newBitmap.getHeight();
        // Fill with black
        for (int x=0; x<xMax; x++) {
            for (int y=0;y<yMax;y++) {
                newBitmap.setPixel(x,y,0x00000000);
            }
        }
        int color=0xFF000000;
        int tempX,tempY;
        for (int x=0;x<xMax;x++) {
            for (int y=0;y<yMax;y++) {
                tempX=(int)Math.round(a00*x+a01*y+b00);
                tempY=(int)Math.round(a10*x+a11*y+b11);
                if ((tempX<xMax) && (tempY<yMax) && (tempX>0) && (tempY>0)) {
                    color = src.getPixel(x,y);
                    newBitmap.setPixel(tempX,tempY,color);
                }
                //newBitmap.setPixel(x,y,color);
            }
        }
        return newBitmap;
    }
}
